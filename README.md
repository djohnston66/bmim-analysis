# BMIM Conformational Analysis

Use the CSD Python API to determine the conformation of the butyl chain and assign labels for any 1-butyl-3-methylimidazolium ions in a set of mol2 files or entry list (gcd).

This script was used to analyze and compare conformations of 1-butyl-3-methylimidazolium nitrate to other published ionic liquid structures. See Johnston, D.H., Gasbarre, M. & Norris, C.E. Structural and Conformational Analysis of 1-Butyl-3-methylimidazolium Nitrate. *J Chem Crystallogr* (2021). https://doi.org/10.1007/s10870-021-00899-w

## Arguments

- input file (required): Name of mol2 (structures) or gcd (refcodes) file
- -o, --output (optional): output file [default=stdout]
- -f, --format (optional): output format, choose from csv or html [default=csv]
