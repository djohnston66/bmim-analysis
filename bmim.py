#!/usr/bin/env python
#
# BMIM Analysis (c) 2020 Dean H. Johnston
# djohnston@otterbein.edu
#
"""Determine the conformation of the butyl chain and assign labels for any
1-butyl-3-methylimidazolium ions in a set of mol2 files or entry list (gcd).

"""
import sys
import csv
import argparse
import ccdc.search
import ccdc.io
from ccdc.descriptors import MolecularDescriptors as MD

import utility as util

def create_entry_reader(path_to_file):
    '''Create entry reader from mol2 or gcd file'''
    entry_reader = ccdc.io.EntryReader(path_to_file)
    return entry_reader

def create_bmim_substructure_search():
    '''Create substructure search for BMIM cation'''

    # atom info:        [    five-membered ring      ]
    # atom numbering:    0       1     2     3     4
    smart_string = '[#6]~1(~[#7](~[#6](~[#6](~[#7]~1-'
    # atom info:        butyl group
    # atom numbering:  5     6     7     8     9     10    11     12    14     15    16
    smart_string += '[#6](-[#6](-[#6](-[#6](-[#1])(-[#1])-[#1])(-[#1])-[#1])(-[#1])-[#1])'
    # atom numbering:   17    18    19    20    21    22     23    24    25
    smart_string += '(-[#1])-[#1])-[#1])-[#1])-[#6](-[#1])(-[#1])-[#1])-[#1]'
    bmim_smarts = ccdc.search.SMARTSSubstructure(smart_string)
    sub_search = ccdc.search.SubstructureSearch()
    sub_id = sub_search.add_substructure(bmim_smarts)
    sub_search.add_torsion_angle_measurement('TOR1', (0,0), (0,4), (0,5), (0,6))
    sub_search.add_torsion_angle_measurement('TOR2', (0,4), (0,5), (0,6), (0,7))
    sub_search.add_torsion_angle_measurement('TOR3', (0,5), (0,6), (0,7), (0,8))
    return sub_search

def get_matched_atoms(hit):
    '''Return dict containing atom N1 and butyl chain atoms and their minimum occupancy'''

    # get atoms from full molecule for distance searching
    full_mol = hit.entry.disordered_molecule   # full molecule, not just hit
    nitrogen = full_mol.atom(hit.match_atoms()[4].label)
    carbon1 = full_mol.atom(hit.match_atoms()[5].label)
    carbon2 = full_mol.atom(hit.match_atoms()[6].label)
    carbon3 = full_mol.atom(hit.match_atoms()[7].label)
    carbon4 = full_mol.atom(hit.match_atoms()[8].label)
    matched_atoms = { 'N': nitrogen, 'C1': carbon1, 'C2': carbon2, 'C3': carbon3, 'C4': carbon4 }
    occupancy = min([a.occupancy for a in matched_atoms.values()])
    return matched_atoms, occupancy

def chain_search(full_mol, atom, length, chain):
    '''Search for next carbon atom in a chain of disordered carbon atoms

    :param full_mol: the full molecule including disordered atoms
    :param atom: the atom from which to seach for nearby atoms
    :param length: how far along the chain to search (end search when length == 0)
    :param chain: list of disordered carbon atoms
    '''
    dist_search = MD.AtomDistanceSearch(full_mol)
    contact = set(dist_search.atoms_within_range(atom.coordinates, 1.7))
    contact = contact - set(a for a in contact if a.atomic_number != 6) \
        - set(a for a in contact if not a.label.endswith('?')) - set(chain)
    if len(contact) == 1 and length > 0:
        chain.append(list(contact)[0])
        chain = chain_search(full_mol, list(contact)[0], length - 1, chain)
    return chain

def get_torsions(bchain, matoms, search_length):
    '''Return torsion angles for the given chain of atoms

    :param bchain: butyl chain of disordered atoms
    :param matoms: (non-disordered) matched atoms from substructures search
    :param chain_length: number of disordered atoms
    '''
    tor2 = 0    # torsion angle phi-2: N1-C7-C8-C9
    tor3 = 0    # torsion angle phi-3: C7-C8-C9-C10
    if search_length == 4:
        if len(bchain) == 4:
            tor2 = MD.atom_torsion_angle(matoms['N'], bchain[0], bchain[1], bchain[2])
            tor3 = MD.atom_torsion_angle(bchain[0], bchain[1], bchain[2], bchain[3])
        elif len(bchain) == 3:
            tor2 = MD.atom_torsion_angle(matoms['N'], bchain[0], bchain[1], bchain[2])
            tor3 = MD.atom_torsion_angle(bchain[0], bchain[1], bchain[2], matoms['C4'])
        elif len(bchain) == 2:
            tor2 = MD.atom_torsion_angle(matoms['N'], bchain[0], bchain[1], matoms['C3'])
            tor3 = MD.atom_torsion_angle(bchain[0], bchain[1], matoms['C3'], matoms['C4'])
        elif len(bchain) == 1:
            tor2 = MD.atom_torsion_angle(matoms['N'], bchain[0], matoms['C2'], matoms['C3'])
            tor3 = MD.atom_torsion_angle(bchain[0], matoms['C2'], matoms['C3'], matoms['C4'])

    elif search_length == 3:
        if len(bchain) == 3:
            tor2 = MD.atom_torsion_angle(matoms['N'], matoms['C1'], bchain[0], bchain[1])
            tor3 = MD.atom_torsion_angle(matoms['C1'], bchain[0], bchain[1], bchain[2])
        if len(bchain) == 2:
            tor2 = MD.atom_torsion_angle(matoms['N'], matoms['C1'], bchain[0], bchain[1])
            tor3 = MD.atom_torsion_angle(matoms['C1'], bchain[0], bchain[1], matoms['C4'])
        if len(bchain) == 1:
            tor2 = MD.atom_torsion_angle(matoms['N'], matoms['C1'], matoms['C2'], bchain[0])
            tor3 = MD.atom_torsion_angle(matoms['C1'], matoms['C2'], bchain[0], matoms['C4'])

    elif search_length == 2:
        if len(bchain) == 2:
            tor2 = MD.atom_torsion_angle(matoms['N'], matoms['C1'], matoms['C2'], bchain[0])
            tor3 = MD.atom_torsion_angle(matoms['C1'], matoms['C2'], bchain[0], bchain[1])
        if len(bchain) == 1:
            tor2 = MD.atom_torsion_angle(matoms['N'], matoms['C1'], matoms['C2'], matoms['C3'])
            tor3 = MD.atom_torsion_angle(matoms['C1'], matoms['C2'], matoms['C3'], bchain[0])

    elif search_length == 1:
        if len(bchain) == 1:
            tor2 = MD.atom_torsion_angle(matoms['N'], matoms['C1'], matoms['C2'], matoms['C3'])
            tor3 = MD.atom_torsion_angle(matoms['C1'], matoms['C2'], matoms['C3'], bchain[0])
    return tor2, tor3

def torsion_search(full_mol, matched_atoms):
    '''Search for butyl chain and return torsions and occupancy

    :param full_mol: full molecule including disordered (suppressed) atoms
    :param matched_atoms: dict of atoms matched in substructure search
    '''
    atoms = list(matched_atoms.values())  # create list of matched atoms
    search_length = 4
    occupancy = 0.0
    while True:
        bchain = chain_search(full_mol, atoms[4-search_length], search_length, [])
        if bchain:
            tor2, tor3 = get_torsions(bchain, matched_atoms, search_length)
            occupancy = bchain[0].occupancy
            break

        search_length = search_length - 1
        if search_length == 0:
            tor2 = 999
            tor3 = 999
            break
    return tor2, tor3, occupancy

def conformer_label(torsion_angle):
    '''Return conformer label for one torsion angle'''

    conf_label = ''
    if torsion_angle < -150 or torsion_angle > 150:
        conf_label += 'T'
    elif -150 <= torsion_angle < -90:
        conf_label += 'E\''
    elif -90 <= torsion_angle < -30:
        conf_label += 'G\''
    elif -30 <= torsion_angle <  30:
        conf_label += 'S'
    elif 30 <= torsion_angle <  90:
        conf_label += 'G'
    elif 90 <= torsion_angle <= 150:
        conf_label += 'E'
    else:
        conf_label += ''
    return conf_label

def create_butyl_conformer_label(tor0, tor1, tor2):
    '''Return a conformer label TT, GT, etc. based on torsion angles

    :params: t0, t1, t2 - three torsion angles
    See Saouane, S.; Norman, S. E.; Hardacre, C.; Fabbiani, F. P. A. Chem. Sci.
    2013, 4 (3), 1270. https://doi.org/10/ggxjv2. for details
    '''
    label = ''
    if tor0 < 0:  # we assume a positive t0
        tor1 = -tor1
        tor2 = -tor2
    if abs(tor0) < 30:  # planar
        label += 'pl-'
    label += conformer_label(tor1)
    label += conformer_label(tor2)
    return label

def write_csv_table(headers, data, stream=sys.stdout):
    """Write CSV table to the given stream."""
    csv_writer = csv.writer(stream)
    csv_writer.writerow(headers)
    for row in data:
        frow = [x if type(x) is str else format(x,'.2f') for x in row]
        csv_writer.writerow(frow)

class Arguments(argparse.ArgumentParser):
    '''Options for the program'''

    def __init__(self):
        argparse.ArgumentParser.__init__(self, description=__doc__)
        self.add_argument(
            'input_file',
            help='Name of mol2 (structures) or gcd (refcodes) file'
        )
        self.add_argument(
            '-o', '--output', default='stdout',
            help='output file [stdout]'
        )
        self.add_argument(
            '-f', '--format', default='csv', choices=['csv', 'html'],
            help='output format [csv]'
        )
        self.args = self.parse_args()

def main():
    """Find and analyze any 1-butyl-3-methylimidazolium ions.
    """
    measurement_list = []
    headers = ['refcode', 'has_disorder', 'occupancy', 'conf_label',
               'torsion1', 'torsion2', 'torsion3', 'space group']
    args = Arguments().args
    mol_reader = create_entry_reader(args.input_file)
    sub_search = create_bmim_substructure_search()
    hits = sub_search.search(database=mol_reader, max_hit_structures=1000)
    for hit in hits:
        full_mol = hit.entry.disordered_molecule   # full molecule, not just hit
        matched_atoms, matched_occupancy = get_matched_atoms(hit)
        tor2, tor3, occupancy = torsion_search(full_mol, matched_atoms)
        conf_label1 = create_butyl_conformer_label(
            hit.measurements['TOR1'], hit.measurements['TOR2'], hit.measurements['TOR3'])

        measurement_list.append(
            [hit.identifier, hit.entry.has_disorder, matched_occupancy, conf_label1,
             hit.measurements['TOR1'], hit.measurements['TOR2'],
             hit.measurements['TOR3'], hit.crystal.spacegroup_symbol])
        if tor2 != 999:
            conf_label2 = create_butyl_conformer_label(hit.measurements['TOR1'], tor2, tor3)
            measurement_list.append(
                [hit.identifier, hit.entry.has_disorder, occupancy, conf_label2,
                 hit.measurements['TOR1'], tor2, tor3, hit.crystal.spacegroup_symbol])

    if args.output == 'stdout':
        fhandle = sys.stdout
    else:
        fhandle = open(args.output, 'w')

    if args.format == 'csv':
        write_csv_table(headers, measurement_list, fhandle)
    elif args.format == 'html':
        util.write_html_table(headers, measurement_list, fhandle)

    if args.output != 'stdout':
        fhandle.close()

if __name__ == '__main__':
    main()
